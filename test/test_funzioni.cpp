#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "../src/funzioni.h"

TEST_CASE("SOmma funziona", ""){
    REQUIRE(somma(0, 0) == 0);
    REQUIRE(somma(2, 3) == 5);
    REQUIRE(somma(2, -2) == 0);
}